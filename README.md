Prvi domaći zadatak za grupu 1

Kreirati aplikaciju za podršku radu agencije za prodaju polovnih vozila. U tu svrhu kreirati
klasu Vozilo koja sadrži
• Zaštićene atribute:
• marku vozila,
• model,
• snagu motora,
• prosečnu potrošnju,
• godinu proizvodnje,
• cenu;

• Javne metode:
• konstruktor koji postavlja vrednosti svih atributa,
• virtuelnu metodu za računanje ocene vozila (ocena se računa po
formuli (5-Cena/10000) + (5-Starost/5) ),
• virtuelnu metodu koja prikazuje sve atribute klase i ocenu vozila na
standardni izlaz;
• Javno svojstvo (property) koje vraća cenu vozila.

Kreirati i sledeće klase izvedene iz klase Vozilo:
• Automobil - koja sadrži dodatne privatne atribute: broj sedišta i broj
vrata,
• Motocikl - koja sadrži dodatni privatni atribut tip (može da uzme
vrednost: Moped, Cruiser, Scooter ili Sport, kreirati enumeraciju sa
ovim vrednostima)
• Kamion – koja sadrži dodatne privatne atribute: visina i nosivost.

U izvedenim klasama definisati konstruktore koji postavljaju sve atribute klasa i
predefinisati metode za izračunavanje ocene i za prikaz na standardni izlaz.
Metoda za prikaz treba da prikaže najpre tip vozila, a zatim sve atribute na ranije opisani
način.

Ocena vozila se kreira na sledeći način:
• Za automobile: OcenaIzOsnovneKlaseVozilo + (10 - ProsečnaPotrošnja)
• Za kamione: OcenaIzOsnovneKlaseVozilo + Nosivost / 2
• Za motocikle: OcenaIzOsnovneKlaseVozilo

U funkciji main kreirati niz Vozila (pretpostaviti da ih ne može biti više od 100 na lageru),
učitati podatke o nekoliko vozila (pre učitavanja podataka o vozilu učitati o kojoj vrsti
vozila se radi), zatim prikazati listu svih vozila na lageru, ukupnu vrednost svih vozila i
najbolje ocenjeno vozilo na lageru.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4
{
    class Vozilo
    {
        const ushort godina = 2019;
        protected string marka;
        protected string model;
        protected float snaga;
        protected float potrosnja;
        protected ushort godinaProizvodnje;
        protected float cena;
        // Konstruktor za postavljanje vrednosti
        public Vozilo(string marka, string model, float snaga, float potrosnja,
            ushort godinaProizvodnje, float cena)
        {
            this.marka = marka;
            this.model = model;
            this.snaga = snaga;
            this.potrosnja = potrosnja;
            this.godinaProizvodnje = godinaProizvodnje;
            this.cena = cena;
        }
        //Konstruktor za kopiranje objekta klase Vozilo
        public Vozilo(Vozilo v)
        {
            this.marka = v.marka;
            this.model = v.model;
            this.snaga = v.snaga;
            this.potrosnja = v.potrosnja;
            this.godinaProizvodnje = v.godinaProizvodnje;
            this.cena = v.cena;
        }
        //Javno svojstvo koji vraca cenu vozila
        public float Cena { get { return this.cena; } }
        //Metoda za izracunavanje ocene vozila
        public virtual float OcenaVozila()
        {
            return (5 - (this.cena / 10000)) + (5 - (Vozilo.godina - this.godinaProizvodnje) / 5);
        }
        //Metoda za prikaz podataka o vozilu
        public virtual void Prikaz()
        {
            Console.WriteLine($" Marka: {this.marka}, Model: {this.model}" +
                $" Snaga: {this.snaga} Prosecna potrosnja: {this.potrosnja}" +
                $" Godina Proizvodnje {this.godinaProizvodnje} Cena: {this.cena}" +
                $" Ocena: {this.OcenaVozila()}");
        }
    }
}

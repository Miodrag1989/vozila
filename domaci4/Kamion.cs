﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4
{
    class Kamion : Vozilo
    {
        private float visina;
        private float nosivost;

        public Kamion(string marka, string model,
            float snaga, float potrosnja, ushort godinaProizvodnje,
            float cena, float visina, float nosivost)
            : base(marka, model, snaga, potrosnja, godinaProizvodnje, cena)
        {
            this.visina = visina;
            this.nosivost = nosivost;
        }

        public Kamion(Kamion k)
            : base(k.marka, k.model, k.snaga, k.potrosnja, k.godinaProizvodnje, k.cena)
        {
            this.visina = k.visina;
            this.nosivost = k.nosivost;
        }

        public override float OcenaVozila()
        {
            return base.OcenaVozila() + (this.nosivost / 2);
        }

        public override void Prikaz()
        {
            Console.WriteLine($"Kamion: Marka: {base.marka}, Model: {base.model}" +
                $" Snaga: {base.snaga} Prosecna potrosnja: {base.potrosnja}" +
                $" Godina Proizvodnje {base.godinaProizvodnje} Cena: {base.cena}" +
                $" Visina {this.visina} Nosivost: {this.nosivost}" +
                $" Ocena: {this.OcenaVozila()}");
        }
    }
}

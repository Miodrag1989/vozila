﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4
{
    class Program
    {
        public static void Ucitaj(out string marka,out string model,out float snaga,
            out float potrosnja, out ushort godina, out float cena)
        {
            Console.WriteLine("Marka vozila:");
            marka = Console.ReadLine();
            Console.WriteLine("Model:");
            model = Console.ReadLine();
            Console.WriteLine("Snaga:");
            float.TryParse(Console.ReadLine(), out snaga);
            Console.WriteLine("Prosecna potrosnja:");
            float.TryParse(Console.ReadLine(), out potrosnja);
            Console.WriteLine("Godina proizvodnje:");
            ushort.TryParse(Console.ReadLine(), out godina);
            Console.WriteLine("Cena:");
            float.TryParse(Console.ReadLine(), out cena);
        }
        public static void Sortiranje(ref Vozilo[] nizVozila,int ukupanBrojUnetihVozila)
        {
            int imin = 0;
            for (int i = 0; i < ukupanBrojUnetihVozila - 1; i++)
            {
                imin = i;
                for (int j = i + 1; j < ukupanBrojUnetihVozila; j++)
                {
                    if (nizVozila[imin].OcenaVozila() < nizVozila[j].OcenaVozila())
                        imin = j;
                }
                if (i != imin)
                {
                    Vozilo pom = nizVozila[imin];
                    nizVozila[imin] = nizVozila[i];
                    nizVozila[i] = pom;
                }
            }
        }
        static void Main(string[] args)
        {
            int broj = 0;
            do
            {
                System.Console.WriteLine("Ukupan broj vozila za unos(1 - 100):");
                int.TryParse(Console.ReadLine(), out broj);
            }while (broj <= 0 || broj > 100);
            Vozilo[] nizVozila = new Vozilo[broj];
            Console.WriteLine("Opcije: 1 - Unos vozila; 2 - Prikaz vozila; 0 - Izlaz");
            int.TryParse(Console.ReadLine(), out int unos);
            int ukupanBrojUnetihVozila = 0, pom = 0;
            while (unos != 0)
            {
                while (unos == 1 && ukupanBrojUnetihVozila != broj)
                {
                    Console.WriteLine("Broj vozila za unos: ");
                    int.TryParse(Console.ReadLine(), out int brojVozilaZaUnos);
                    for (int i = ukupanBrojUnetihVozila; i < brojVozilaZaUnos + pom && i < nizVozila.Length; i++)
                    {
                        Console.WriteLine("Vrsta vozila(1 - Automobil; 2 - Kamion; 3 - Motocikl):");
                        int.TryParse(Console.ReadLine(), out int vrsta);
                        while (vrsta < 1 || vrsta > 3)
                        {
                            Console.WriteLine("Vrsta vozila(1 - Automobil; 2 - Kamion; 3 - Motocikl):");
                            int.TryParse(Console.ReadLine(), out vrsta);
                        }
                        switch (vrsta)
                        {
                            case 1:
                                Console.WriteLine("Ucitavanje podataka o automobilu!");
                                Ucitaj(out string marka, out string model, out float snaga, out float potrosnja, out ushort godina, out float cena);
                                Console.WriteLine("Broj sedista:");
                                byte.TryParse(Console.ReadLine(), out byte brojSedista);
                                Console.WriteLine("Broj vrata:");
                                byte.TryParse(Console.ReadLine(), out byte brojVrata);
                                nizVozila[i] = new Automobil(marka, model, snaga, potrosnja, godina, cena, brojSedista, brojVrata);
                                break;
                            case 2:
                                Console.WriteLine("Ucitavanje podataka o kamionu!");
                                Ucitaj(out marka, out model, out snaga, out potrosnja, out godina, out cena);
                                Console.WriteLine("Visina:");
                                float.TryParse(Console.ReadLine(), out float visina);
                                Console.WriteLine("Nosivost:");
                                float.TryParse(Console.ReadLine(), out float nosivost);
                                nizVozila[i] = new Kamion(marka, model, snaga, potrosnja, godina, cena, visina, nosivost);
                                break;
                            case 3:
                                Console.WriteLine("Ucitavanje podataka o motociklu!");
                                Ucitaj(out marka, out model, out snaga, out potrosnja, out godina, out cena);
                                Console.WriteLine("Tip motocikla(1 - Moped; 2 - Cruiser; 3 - Scooter; 4 - Sport):");
                                int.TryParse(Console.ReadLine(), out int tip);
                                while (tip < 1 || tip > 4)
                                {
                                    Console.WriteLine("Tip motocikla(1 - Moped; 2 - Cruiser; 3 - Scooter; 4 - Sport):");
                                    int.TryParse(Console.ReadLine(), out tip);
                                }
                                switch (tip)
                                {
                                    case 1:
                                        Enumeracije.Motocikli tipMotocikla = Enumeracije.Motocikli.Moped;
                                        nizVozila[i] = new Motocikl(marka, model, snaga, potrosnja, godina, cena, tipMotocikla);
                                        break;
                                    case 2:
                                        tipMotocikla = Enumeracije.Motocikli.Cruiser;
                                        nizVozila[i] = new Motocikl(marka, model, snaga, potrosnja, godina, cena, tipMotocikla);
                                        break;
                                    case 3:
                                        tipMotocikla = Enumeracije.Motocikli.Scooter;
                                        nizVozila[i] = new Motocikl(marka, model, snaga, potrosnja, godina, cena, tipMotocikla);
                                        break;
                                    case 4:
                                        tipMotocikla = Enumeracije.Motocikli.Sport;
                                        nizVozila[i] = new Motocikl(marka, model, snaga, potrosnja, godina, cena, tipMotocikla);
                                        break;
                                }
                                break;
                        }
                    }
                    pom = ukupanBrojUnetihVozila += brojVozilaZaUnos;

                    Console.WriteLine("Opcije: 1 - Unos vozila; 2 - Prikaz vozila; 0 - Izlaz");
                    int.TryParse(Console.ReadLine(), out unos);
                }
                while (ukupanBrojUnetihVozila == broj && unos == 1)
                {
                    Console.WriteLine("Lager je pun!");
                    Console.WriteLine("Opcije: 1 - Unos vozila; 2 - Prikaz vozila; 0 - Izlaz");
                    int.TryParse(Console.ReadLine(), out unos);
                }
                while (unos == 2)
                {
                    Console.WriteLine("Lager:");
                    float ukupnaVrednostVozila = 0;
                    for (int i = 0; i < ukupanBrojUnetihVozila; i++)
                    {
                        nizVozila[i].Prikaz();
                        ukupnaVrednostVozila += nizVozila[i].Cena;
                    }
                    Sortiranje(ref nizVozila, ukupanBrojUnetihVozila);
                    Console.WriteLine();
                    Console.WriteLine($"Ukupna vrednost vozila je {ukupnaVrednostVozila}.");
                    Console.WriteLine("Najbolja ocenu na lageru ima vozilo:");
                    nizVozila[0].Prikaz();
                    Console.WriteLine();
                    Console.WriteLine("Opcije: 1 - Unos vozila; 2 - Prikaz vozila; 0 - Izlaz");
                    int.TryParse(Console.ReadLine(), out unos);

                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4
{
    class Motocikl : Vozilo
    {

        private Enumeracije.Motocikli tip;


        public Motocikl(string marka, string model,
            float snaga, float potrosnja, ushort godinaProizvodnje,
            float cena, Enumeracije.Motocikli tip)
            : base(marka, model, snaga, potrosnja, godinaProizvodnje, cena)
        {
            this.tip = tip;
        }

        public Motocikl(Motocikl m)
            : base(m.marka, m.model, m.snaga, m.potrosnja, m.godinaProizvodnje, m.cena)
        {
            this.tip = m.tip;
        }

        public override float OcenaVozila()
        {
            return base.OcenaVozila();
        }

        public override void Prikaz()
        {
            Console.WriteLine($"Motocikl: Tip: {this.tip} Marka: {base.marka}, " +
                $" Model: {base.model} Snaga: {base.snaga} Prosecna potrosnja: {base.potrosnja}" +
                $" Godina Proizvodnje {base.godinaProizvodnje} Cena: {base.cena}" +
                $" Ocena: {this.OcenaVozila()}");
        }
    }
}

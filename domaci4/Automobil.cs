﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4
{
    class Automobil : Vozilo
    {
        private byte brojSedista;
        private byte brojVrata;

        public Automobil(string marka,string model,
            float snaga, float potrosnja, ushort godinaProizvodnje,
            float cena, byte brojSedista, byte brojVrata)
            : base(marka,model,snaga,potrosnja,godinaProizvodnje,cena)
        {
            this.brojSedista = brojSedista;
            this.brojVrata = brojVrata;
        }

        public Automobil(Automobil a)
            : base(a.marka, a.model, a.snaga, a.potrosnja, a.godinaProizvodnje, a.cena)
        {
            this.brojSedista = a.brojSedista;
            this.brojVrata = a.brojVrata;
        }
        public override float OcenaVozila()
        {
            return base.OcenaVozila() + (10 - base.potrosnja);
        }
        public override void Prikaz()
        {
            Console.WriteLine($"Automobil: Marka: {base.marka}, Model: {base.model}" +
                $" Snaga: {base.snaga} Prosecna potrosnja: {base.potrosnja}" +
                $" Godina Proizvodnje {base.godinaProizvodnje}" +
                $" Broj vrata: {this.brojVrata} Broj Sedista: {this.brojSedista} Cena: {base.cena}" +
                $" Ocena: {this.OcenaVozila()}"); 
        }
    }
}
